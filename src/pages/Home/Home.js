import React, { useState } from 'react';

import axios from 'axios';

const Home = () => {
  const [value, setValue] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [password, setPassword] = useState('');
  const isValidURL = url => {
    try {
      return Boolean(new URL(url));
    } catch (err) {
      return false;
    }
  };

  const getStr = str => {
    if (str === 'cdn.taboola.com') {
      return ['Taboola', 'blue'];
    }
    if (str === 'go.ezoic.net') {
      return ['Ezoic', 'grey'];
    }
    if (str === 'googlesyndication') {
      return ['Google Adsense', 'green'];
    }
  };

  const handleLogin = () => {
    if (password === process.env.REACT_APP_PASSWORD) {
      setIsLoggedIn(true);
    } else {
      alert('Please enter correct password!');
    }
  };

  const handleSearch = async () => {
    setError(null);
    setResults(null);
    const listOfURLs = value.split(',');
    // eslint-disable-next-line
    const validURLs = listOfURLs.filter(url => {
      if (isValidURL(url)) {
        return url;
      }
    });
    if (listOfURLs.length !== validURLs.length) {
      return setError('Please enter valid URLs');
    }
    if (validURLs.length > 5) {
      return setError('Maximum 5 urls allowed per request!');
    }
    try {
      setLoading(true);
      const { data } = await axios.post('https://scanner.1inqloop.com/', {
        urls: validURLs,
        keywords: ['googlesyndication', 'cdn.taboola.com', 'go.ezoic.net'],
      });
      const result = [];
      data?.forEach(res => {
        const url = res?.url;
        const urlFound = [];
        res?.response.forEach(item => {
          const itemKey = Object.keys(item)[0];
          if (urlFound.indexOf(itemKey) > -1) {
            return;
          } else {
            urlFound.push(itemKey);
          }
        });
        result.push({ [url]: urlFound });
      });
      console.log(result);
      setResults(result);
    } catch (err) {
      console.log(err.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className='container'>
      <div className='wrapper'>
        <div className='inner'>
          {isLoggedIn ? (
            <>
              <div className='header'>
                <h2 className='title'>Add network finder</h2>
              </div>
              <div className='body'>
                <h1 className='title'>Scan a webpage for adnetwork</h1>
                <p className='label'>Search from URL</p>
                <textarea
                  className={error && 'error'}
                  value={value}
                  onChange={e => setValue(e.target.value)}
                  placeholder='https://www.yahoo.com'
                ></textarea>
                {error && <p className='error'>{error}</p>}

                <div className='btn-container'>
                  <button
                    disabled={loading}
                    className='btn'
                    onClick={handleSearch}
                  >
                    Search
                  </button>
                </div>

                <div className='results'>
                  {results?.map((r, index) => (
                    <div
                      key={index}
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        gap: '10px',
                      }}
                    >
                      <h3>{Object.keys(r)[0]}</h3>
                      {r[Object.keys(r)[0]].length === 0 && (
                        <p className='res-item'>Not found</p>
                      )}

                      {r[Object.keys(r)[0]].map(str => {
                        console.log(r[Object.keys(r)[0]]);
                        return (
                          <p
                            className={'res-item'}
                            style={{ background: getStr(str)[1] }}
                            key={str}
                          >
                            {getStr(str)[0]}
                          </p>
                        );
                      })}
                    </div>
                  ))}
                </div>
              </div>
            </>
          ) : (
            <div className='dialog'>
              <h2 className='title'>Please enter password</h2>
              <input
                type='password'
                value={password}
                onChange={e => setPassword(e.target.value)}
              />
              <button onClick={handleLogin}>Login</button>
            </div>
          )}
        </div>
        {loading && (
          <div className='loader'>
            <div className='loaderIndicator'>
              <div class='lds-spinner'>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
              <h2>Please wait while we are processing your request!</h2>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Home;
